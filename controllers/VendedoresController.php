<?php

namespace app\controllers;

use app\models\Vendedores;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * VendedoresController implements the CRUD actions for Vendedores model.
 */
class VendedoresController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Vendedores models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Vendedores::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'IdVendedor' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionIndexg()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Vendedores::find(),
            
            'pagination' => [
                'pageSize' => 5
            ],
            'sort' => [
                'defaultOrder' => [
                    'IdVendedor' => SORT_DESC,
                ]
            ],
            
        ]);

        return $this->render('indexg', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Vendedores model.
     * @param int $IdVendedor Id Vendedor
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($IdVendedor)
    {
        $dataProvider=new ActiveDataProvider([
                "query" => $this->findModel($IdVendedor)->getVentas(), // consulta sin ejecutar
                'pagination' => [
                    'pageSize' => 5
            ],
        ]);

        return $this->render('view', [
            'model' => $this->findModel($IdVendedor),
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Vendedores model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Vendedores();
        

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'IdVendedor' => $model->IdVendedor]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Vendedores model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $IdVendedor Id Vendedor
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($IdVendedor)
    {
        $model = $this->findModel($IdVendedor);
        
        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'IdVendedor' => $model->IdVendedor]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Vendedores model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $IdVendedor Id Vendedor
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($IdVendedor)
    {
        $this->findModel($IdVendedor)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Vendedores model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $IdVendedor Id Vendedor
     * @return Vendedores the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($IdVendedor)
    {
        if (($model = Vendedores::findOne(['IdVendedor' => $IdVendedor])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

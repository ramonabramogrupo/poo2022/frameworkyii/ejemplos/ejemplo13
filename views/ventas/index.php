<?php

use app\models\Ventas;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */
$this->title = 'Ventas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ventas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Tabla', ['indexg'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Nueva venta', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    yii\widgets\ListView::widget([
        "dataProvider" => $dataProvider,
        "itemView" => "_ver",
        "itemOptions" => [
            'class' => 'col-lg-4 ml-auto mr-auto bg-light p-3 mb-5',
        ],
        "options" => [
            'class' => 'row',
        ],
        'layout' => "{items}{pager}",
    ])
    ?>


</div>

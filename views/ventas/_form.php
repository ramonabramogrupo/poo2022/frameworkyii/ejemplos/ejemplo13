<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Ventas $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="ventas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form
            ->field($model, 'CodVendedor')
            ->dropDownList($model->vendedores())
    ?>

    <?= $form
            ->field($model, 'CodProducto')
            ->listBox($model->productos())
    ?>

    <?= $form->field($model, 'Fecha')->input("datetime-local") ?>

    <?= $form->field($model, 'Kilos')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

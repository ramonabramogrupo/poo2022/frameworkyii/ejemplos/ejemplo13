<div class="row">
    <div class="col-lg-12">
        <h4 class="m-2">Codigo de la venta: <?= $model->idventas ?><br></h4>
        <?php
            $items = [
                [
                    "header" => "Vendedor",
                    "content" => $model->vendedor
                ],
                [
                    "header" => "Producto",
                    "content" => $model->producto
                ],
            ];
        
            echo yii\jui\Accordion::widget([
                'items' => $items,
                'options' => ['tag' => 'div'],
                'itemOptions' => ['tag' => 'div','class'=>'p-2'],
                'headerOptions' => ['tag' => 'h3'],
                'clientOptions' => [
                    'collapsible' => true,
                    'active'=>false,
                ],
            ]);
        ?>
        
        <div class="text-white bg-primary rounded p-2">Fecha:</div>
        <div class="p-1"><?= $model->FechaFinal ?></div>
        <div class="text-white bg-primary rounded p-2">Kilos:</div>
        <div class="p-1"><?= $model->Kg ?></div>
        
        <?php
        
       

        
        
        /*echo yii\bootstrap4\Accordion::widget([
            'items' => $items1,
            'options' => ['tag' => 'div'],
            'autoCloseItems' => false,
            'clientOptions' => [
                'collapsible' => true,
                'active' => false,
                'classes' =>[
                    "ui-accordion-header"=>"bg-primary",
                ]
            ],
        ]);*/
        ?>
        <div class="p-1 mb-5">
        <?php
        // BOTON DE VER
        echo yii\helpers\Html::a(
                '<i class="fal fa-eye"></i>', // icono
                ['view', 'idventas' => $model->idventas], // controlador/accion y parametro
                ['class' => 'btn btn-primary mr-2'] // estilos del boton
        );

        // BOTON DE ACTUALIZAR
        echo yii\helpers\Html::a(
                '<i class="fad fa-pencil-alt"></i> ', // icono
                ['update', 'idventas' => $model->idventas], // controlador/accion y parametro
                ['class' => 'btn btn-primary mr-2'] // estilos
        );

        // BOTON DE ELIMINAR
        echo yii\helpers\Html::a(
                '<i class="fad fa-trash"></i>', // icono
                ['delete', 'idventas' => $model->idventas], // controlador/accion y parametro
                [
                    'class' => 'btn btn-primary', // estilos
                    'data' => [
                        'confirm' => '¿Estas seguro que deseas eliminar el registro?',
                        'method' => 'post',
                    ], // esto solo para boton eliminar
                ],
        );
        ?>
        </div>
        <br class="float-none">
    </div>
</div>




<?php

use app\models\Ventas;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Ventas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ventas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Tarjeta', ['index'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Nueva venta', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idventas',
            'CodVendedor',
            'CodProducto',
            /*[
                'attribute' => 'Fecha',
                'value' => function ($model){
                    if(!empty($model->Fecha)){
                        return Yii::$app->formatter->asDate($model->Fecha, 'php:d/m/Y');
                    }
                }
            ],*/
            //'FechaFinal',
            [
                'attribute' => 'Fecha',
                //'value' => 'FechaFinal',
                'value' => function($model){
                    return $model->FechaFinal;
                }
            ],
            //'Kilos',
            [
                'attribute' => "Kilos",
                'value' => function($model){
                    return $model->Kg;
                }
            ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Ventas $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'idventas' => $model->idventas]);
                 }
            ],
        ],
    ]); ?>


</div>

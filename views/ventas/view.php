<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Ventas $model */

$this->title = $model->idventas;
$this->params['breadcrumbs'][] = ['label' => 'Ventas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="ventas-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'idventas' => $model->idventas], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'idventas' => $model->idventas], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idventas',
            //'CodVendedor',
            /*[
                'attribute' => 'CodVendedor',
                'format' => 'raw',
                'value' => function($model){
                    return Html::ul($model->codVendedor);
                }
            ],*/
            [
                'attribute' => 'vendedor',
                'format' => 'raw',
            ],
            //'CodProducto',
            [
                'attribute' => 'producto',
                'format' => 'raw',
            ],
            [
                'attribute' => 'Fecha',
                'value' => function ($model){
                    if(!empty($model->Fecha)){
                        return Yii::$app->formatter->asDate($model->Fecha, 'php:d/m/Y');
                    }
                }
            ],
            'Kilos',
        ],
    ]) ?>

</div>

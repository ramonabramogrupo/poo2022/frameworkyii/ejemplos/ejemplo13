<div class="row">
    <div class="col-lg-12">
        <h3>Id: <?= $model->IdGrupo ?><br></h3>
        <div class="text-white bg-primary rounded p-2">Nombre:</div>
        <div class="p-1"><?= $model->NombreGrupo ?></div>
        <div class="p-1 mb-5">
            <?php
            // BOTON DE VER
            echo yii\helpers\Html::a(
                    '<i class="fal fa-eye"></i>', // icono
                    ['view', 'IdGrupo' => $model->IdGrupo], // controlador/accion y parametro
                    ['class' => 'btn btn-primary mr-2'] // estilos del boton
            );

            // BOTON DE ACTUALIZAR
            echo yii\helpers\Html::a(
                    '<i class="fad fa-pencil-alt"></i> ', // icono
                    ['update', 'IdGrupo' => $model->IdGrupo], // controlador/accion y parametro
                    ['class' => 'btn btn-primary mr-2'] // estilos
            );

            // BOTON DE ELIMINAR
            echo yii\helpers\Html::a(
                    '<i class="fad fa-trash"></i>', // icono
                    ['delete', 'IdGrupo' => $model->IdGrupo], // controlador/accion y parametro
                    [
                        'class' => 'btn btn-primary', // estilos
                        'data' => [
                            'confirm' => '¿Estas seguro que deseas eliminar el registro?',
                            'method' => 'post',
                        ], // esto solo para boton eliminar
                    ],
            );
            ?>
        </div>
        <br class="float-none">
    </div>
</div>



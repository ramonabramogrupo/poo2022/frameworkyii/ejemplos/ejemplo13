<?php

use app\models\Grupos;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Grupos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grupos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Tarjeta', ['index'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <p>
        <?= Html::a('+', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'IdGrupo',
            'NombreGrupo',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Grupos $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'IdGrupo' => $model->IdGrupo]);
                 }
            ],
        ],
    ]); ?>


</div>

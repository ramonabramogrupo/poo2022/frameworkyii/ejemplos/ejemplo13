<?php

use app\models\Productos;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Productos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="productos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Tarjeta', ['index'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('+', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'IdProducto',
            'NomProducto',
            'IdGrupo',
            //'idGrupo.NombreGrupo',
            [
                'label' => 'Nombre del grupo',
                'value' => function($model){
                    return $model->idGrupo->NombreGrupo;
                }
            ],
            //'Precio',
            // 'precioFinal',
            [
              'attribute' => 'Precio',
               'value' => function($model){
                    return $model->precioFinal;
               }
            ],
            [
                'attribute' => 'foto',
                'content' => function($model) {
                    $nombre="anonimo.png";
                    
                    if(isset($model->foto) && $model->foto!=""){
                        $nombre="$model->IdProducto/$model->foto";
                    }
                    
                    return Html::img(
                            "@web/imgs/$nombre",
                            [
                                "class" => "img-thumbnail"
                            ]
                    );
                }
            ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Productos $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'IdProducto' => $model->IdProducto]);
                 },
                 'options' => [
                      "class" => "col-2"
                 ]
            ],
        ],
    ]); ?>


</div>

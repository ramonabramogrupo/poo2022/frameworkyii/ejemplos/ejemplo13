<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use app\models\Ventas;
use yii\helpers\Url;


/** @var yii\web\View $this */
/** @var app\models\Productos $model */

$this->title = $model->IdProducto;
$this->params['breadcrumbs'][] = ['label' => 'Productos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="productos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'IdProducto' => $model->IdProducto], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'IdProducto' => $model->IdProducto], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'IdProducto',
            'NomProducto',
            'IdGrupo',
            [
               'label' => 'Nombre Grupo',
               'value' => function($model){
                    return $model->idGrupo->NombreGrupo;
               }
            ],
            //'idGrupo.NombreGrupo',
            //'Precio',
            'precioFinal',
            [
                'label' => 'foto',
                'format' => 'raw',
                'value' => function($model) {
                    $nombre="anonimo.png";
                    
                    if(isset($model->foto) && $model->foto!=""){
                        $nombre="$model->IdProducto/$model->foto";
                    }
                    
                    return Html::img(
                            "@web/imgs/$nombre",
                            [
                                "class" => "img-thumbnail col-6"
                            ]
                    );
                }
            ],
        ],
    ]) ?>
    
    
     <?php yii\widgets\Pjax::begin() ?>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idventas',
            'CodVendedor',
            [
                'attribute' => 'Fecha',
                //'value' => 'FechaFinal',
                'value' => function($model){
                    return $model->FechaFinal;
                }
            ],
            [
                'attribute' => "Kilos",
                'value' => function($model){
                    return $model->Kg;
                }
            ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Ventas $model, $key, $index, $column) {
                    return Url::toRoute(['ventas/' . $action, 'idventas' => $model->idventas]);
                 }
            ],
        ],
    ]); ?>

    <?php yii\widgets\Pjax::end() ?>

</div>

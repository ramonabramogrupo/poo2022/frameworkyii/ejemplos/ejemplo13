<div class="row">
    <div class="col-lg-12">
        <h3>Id: <?= $model->IdProducto ?><br></h3>
        <div class="text-white bg-primary rounded p-2">Nombre:</div>
        <div class="p-1"><?= $model->NomProducto ?></div>
        <div class="text-white bg-primary rounded p-2">Grupo:</div>
        <div class="p-1"><?= $model->IdGrupo ?></div>
        <div class="p-1"><?= $model->idGrupo->NombreGrupo ?></div>
        <div class="text-white bg-primary rounded p-2">Precio:</div>
        <div class="p-1"><?= $model->precioFinal ?></div>
        <div class="text-white bg-primary rounded p-2">Foto:</div>
        <div class="p-1">
        <?php  
            if(isset($model->foto)&& $model->foto!=""){
                echo \yii\helpers\Html::img(
                  "@web/imgs/$model->IdProducto/$model->foto",
                  [
                      "class" => "img-thumbnail"
                  ]
                );
            }else{
                echo \yii\helpers\Html::img(
                  "@web/imgs/anonimo.png",
                  [
                      "class" => "img-thumbnail"
                  ]
                );
            }
        ?>
        </div>
        <div class="p-1 mb-5">
            <?php
            // BOTON DE VER
            echo yii\helpers\Html::a(
                    '<i class="fal fa-eye"></i>', // icono
                    ['view', 'IdProducto' => $model->IdProducto], // controlador/accion y parametro
                    ['class' => 'btn btn-primary mr-2'] // estilos del boton
            );

            // BOTON DE ACTUALIZAR
            echo yii\helpers\Html::a(
                    '<i class="fad fa-pencil-alt"></i> ', // icono
                    ['update', 'IdProducto' => $model->IdProducto], // controlador/accion y parametro
                    ['class' => 'btn btn-primary mr-2'] // estilos
            );

            // BOTON DE ELIMINAR
            echo yii\helpers\Html::a(
                    '<i class="fad fa-trash"></i>', // icono
                    ['delete', 'IdProducto' => $model->IdProducto], // controlador/accion y parametro
                    [
                        'class' => 'btn btn-primary', // estilos
                        'data' => [
                            'confirm' => '¿Estas seguro que deseas eliminar el registro?',
                            'method' => 'post',
                        ], // esto solo para boton eliminar
                    ],
            );
            ?>
        </div>
        <br class="float-none">
    </div>
</div>



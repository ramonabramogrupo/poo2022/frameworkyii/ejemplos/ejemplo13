<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Productos $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="productos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'NomProducto')->textInput(['maxlength' => true]) ?>
    
    <?php 
            /*echo $form
                ->field($model, 'IdGrupo')
                ->dropDownList($model->grupos());*/
    ?>

    <?php
        /*echo $form
                ->field($model, 'IdGrupo')
                ->widget(yii\jui\AutoComplete::className(), [
                        'options' => ['class'=>'form-control'],
                        'clientOptions' => [
                                'source' => $model->gruposAutompletar(),
                        ],
                ]); */
    ?>

    <?php
        echo $form
            ->field($model, 'IdGrupo')
            ->widget(\kartik\select2\Select2::className(), [
                'data' => $model->grupos(),
                'options' => ['placeholder' => 'Selecciona un grupo'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
        ]);
    ?>

    <?= 
            $form
                ->field($model, 'Precio')
                ->input("number")
    ?>

    <?= $form->field($model, 'foto')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use app\models\Ventas;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var app\models\Vendedores $model */

$this->title = $model->IdVendedor;
$this->params['breadcrumbs'][] = ['label' => 'Vendedores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="vendedores-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'IdVendedor' => $model->IdVendedor], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'IdVendedor' => $model->IdVendedor], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'IdVendedor',
            'NombreVendedor',
            //'FechaAlta',
            //'FechaAltaFinal',
            [
                'attribute' => 'FechaAlta',
                'value' => function($model){
                    return $model->FechaAltaFinal;
                }
            ],
            'NIF',
            //'FechaNac',
            'FechaNacimiento',
            /*[
                'attribute' => 'FechaNac',
                'value' => function($model){
                    return $model->FechaNacimiento;
                }
            ],*/
            'Direccion',
            'Poblacion',
            'CodPostal',
            'Telefon',
            'EstalCivil',
            'activo:boolean',
        ],
    ]) ?>
    <?php yii\widgets\Pjax::begin() ?>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idventas',
            'CodProducto',
            [
                'attribute' => 'Fecha',
                //'value' => 'FechaFinal',
                'value' => function($model){
                    return $model->FechaFinal;
                }
            ],
            [
                'attribute' => "Kilos",
                'value' => function($model){
                    return $model->Kg;
                }
            ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Ventas $model, $key, $index, $column) {
                    return Url::toRoute(['ventas/' . $action, 'idventas' => $model->idventas]);
                 }
            ],
        ],
    ]); ?>

    <?php yii\widgets\Pjax::end() ?>
    

</div>

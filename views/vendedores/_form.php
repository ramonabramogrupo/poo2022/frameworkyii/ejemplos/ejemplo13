<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Vendedores $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="vendedores-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'NombreVendedor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'FechaAlta')->input("datetime-local") ?>

    <?= $form->field($model, 'NIF')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'FechaNac')->input("datetime-local") ?>

    <?= $form->field($model, 'Direccion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Poblacion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CodPostal')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Telefon')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'EstalCivil')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'activo')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

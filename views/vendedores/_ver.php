<div class="row">
    <div class="col-lg-12">
        <h3>IdVendedor: <?= $model->IdVendedor ?><br></h3>
        <div class="text-white bg-primary rounded p-2">Nombre:</div>
        <div class="p-1"><?= $model->NombreVendedor ?></div>
        <div class="text-white bg-primary rounded p-2">Fecha Alta:</div>
        <div class="p-1"><?= $model->FechaAltaFinal ?></div>
        <div class="text-white bg-primary rounded p-2">NIF:</div>
        <div class="p-1"><?= $model->NIF ?></div>
        <div class="text-white bg-primary rounded p-2">Mas informacion:</div>
        <?php
        $items = [
            [
                "header" => "Nombre del vendedor",
                "content" => $model->NombreVendedor
            ],
            [
                "header" => "Fecha Nacimiento",
                "content" => $model->FechaNacimiento
            ],
            [
                "header" => "Direccion",
                "content" => $model->Direccion
            ],
        ];
        
        $items1 = [
            [
                "label" => "Nombre del vendedor",
                "content" => "$model->NombreVendedor"
            ],
            [
                "label" => "Fecha Nacimiento",
                "content" => "$model->FechaNacimiento"
            ],
            [
                "label" => "Direccion",
                "content" => "$model->Direccion"
            ],
        ];

        echo yii\jui\Accordion::widget([
            'items' => $items,
            'options' => ['tag' => 'div'],
            'itemOptions' => ['tag' => 'div','class'=>'p-2'],
            'headerOptions' => ['tag' => 'h3'],
            'clientOptions' => [
                'collapsible' => true,
                'active'=>false,
            ],
        ]);
        
        /*echo yii\bootstrap4\Accordion::widget([
            'items' => $items1,
            'options' => ['tag' => 'div'],
            'autoCloseItems' => false,
            'clientOptions' => [
                'collapsible' => true,
                'active' => false,
                'classes' =>[
                    "ui-accordion-header"=>"bg-primary",
                ]
            ],
        ]);*/
        ?>
        <div class="p-1 mb-5">
        <?php
        // BOTON DE VER
        echo yii\helpers\Html::a(
                '<i class="fal fa-eye"></i>', // icono
                ['view', 'IdVendedor' => $model->IdVendedor], // controlador/accion y parametro
                ['class' => 'btn btn-primary mr-2'] // estilos del boton
        );

        // BOTON DE ACTUALIZAR
        echo yii\helpers\Html::a(
                '<i class="fad fa-pencil-alt"></i> ', // icono
                ['update', 'IdVendedor' => $model->IdVendedor], // controlador/accion y parametro
                ['class' => 'btn btn-primary mr-2'] // estilos
        );

        // BOTON DE ELIMINAR
        echo yii\helpers\Html::a(
                '<i class="fad fa-trash"></i>', // icono
                ['delete', 'IdVendedor' => $model->IdVendedor], // controlador/accion y parametro
                [
                    'class' => 'btn btn-primary', // estilos
                    'data' => [
                        'confirm' => '¿Estas seguro que deseas eliminar el registro?',
                        'method' => 'post',
                    ], // esto solo para boton eliminar
                ],
        );
        ?>
        </div>
        <br class="float-none">
    </div>
</div>




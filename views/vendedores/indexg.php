<?php

use app\models\Vendedores;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Vendedores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vendedores-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<i class="fas fa-th-large"></i> Tarjeta', ['index'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="fas fa-user-plus"></i> Nuevo Vendedor', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'IdVendedor',
            'NombreVendedor',
            //'FechaAlta',
            //'FechaAltaFinal',
            [
                'attribute' => 'FechaAlta',
                'content' => function($model){
                    return $model->FechaAltaFinal;
                }
            ],
            /*[
                'attribute' => 'FechaAlta',
                'value' => function ($model){
                    if(!empty($model->FechaAlta)){
                        return Yii::$app->formatter->asDate($model->FechaAlta, 'php:d/m/Y');
                    }
                }
            ],*/
                    
            'NIF',
            //'FechaNac',
            [
                'attribute' => "FechaNac",
                "value" => function($model){
                    return $model->FechaNacimiento;
                }
            ],
            //'Direccion',
            //'Poblacion',
            //'CodPostal',
            //'Telefon',
            //'EstalCivil',
            //'activo:boolean',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Vendedores $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'IdVendedor' => $model->IdVendedor]);
                 }
            ],
        ],
    ]); ?>


</div>

<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Vendedores $model */

$this->title = 'Update Vendedores: ' . $model->IdVendedor;
$this->params['breadcrumbs'][] = ['label' => 'Vendedores', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->IdVendedor, 'url' => ['view', 'IdVendedor' => $model->IdVendedor]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="vendedores-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ventas".
 *
 * @property int $idventas
 * @property int $CodVendedor
 * @property int $CodProducto
 * @property string $Fecha
 * @property float|null $Kilos
 *
 * @property Productos $codProducto
 * @property Vendedores $codVendedor
 */
class Ventas extends \yii\db\ActiveRecord
{
    private string $FechaFinal;
    private string $Kg;
    private string $vendedor;
    private string $producto;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ventas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CodVendedor', 'CodProducto', 'Fecha'], 'required'],
            [['CodVendedor', 'CodProducto'], 'integer'],
            [['Fecha'], 'safe'],
            [['Kilos'], 'number'],
            [['CodProducto'], 'exist', 'skipOnError' => true, 'targetClass' => Productos::class, 'targetAttribute' => ['CodProducto' => 'IdProducto']],
            [['CodVendedor'], 'exist', 'skipOnError' => true, 'targetClass' => Vendedores::class, 'targetAttribute' => ['CodVendedor' => 'IdVendedor']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idventas' => 'Idventas',
            'CodVendedor' => 'Cod Vendedor',
            'CodProducto' => 'Cod Producto',
            'Fecha' => 'Fecha',
            'Kilos' => 'Kilos',
        ];
    }

    /**
     * Gets query for [[CodProducto]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodProducto()
    {
        return $this->hasOne(Productos::class, ['IdProducto' => 'CodProducto']);
    }

    /**
     * Gets query for [[CodVendedor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodVendedor()
    {
        return $this->hasOne(Vendedores::class, ['IdVendedor' => 'CodVendedor']);
    }
    
    public function productos(){
        $listado=Productos::find()->all();
        return \yii\helpers\ArrayHelper::map($listado,'IdProducto','NomProducto');
    }
    
    public function vendedores(){
        $listado= Vendedores::find()->all();
        return \yii\helpers\ArrayHelper::map($listado,"IdVendedor","NombreVendedor");
    }
    
    public function getFechaFinal(): string {
         if (!empty($this->Fecha)) {
            return Yii::$app->formatter->asDate($this->Fecha, 'php:d/m/Y');
        }
        
    }
    
    public function getKg(): string {
        return $this->Kilos . " KG";
    }
    
    public function getVendedor(){
        return $this->CodVendedor 
                . "<br><br>" 
                . $this->codVendedor->NombreVendedor
                . "<br><br>"
                . $this->codVendedor->NIF
                . "<br><br>"
                . \yii\helpers\Html::a(
                        'Mas informacion',
                        ['vendedores/view','IdVendedor' => $this->CodVendedor],
                        ['class'=>'btn btn-primary']
                );
    }
    
    public function getProducto(){
        return $this->CodProducto
                . "<br><br>"
                . $this->codProducto->NomProducto
                . "<br><br>"
                . $this->codProducto->precioFinal
                . "<br><br>"
                . \yii\helpers\Html::a(
                        'Mas informacion',
                        ['productos/view','IdProducto' => $this->CodProducto],
                        ['class'=>'btn btn-primary']
                );
    }



    
    
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productos".
 *
 * @property int $IdProducto
 * @property string|null $NomProducto
 * @property int|null $IdGrupo
 * @property float|null $Precio
 * @property string|null $foto
 *
 * @property Grupos $idGrupo
 * @property Ventas[] $ventas
 */
class Productos extends \yii\db\ActiveRecord
{
    private string $precioFinal;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['IdGrupo'], 'integer'],
            [['Precio'], 'number'],
            [['NomProducto'], 'string', 'max' => 50],
            [['foto'], 'string', 'max' => 100],
            [['IdGrupo'], 'exist', 'skipOnError' => true, 'targetClass' => Grupos::class, 'targetAttribute' => ['IdGrupo' => 'IdGrupo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IdProducto' => 'Id Producto',
            'NomProducto' => 'Nom Producto',
            'IdGrupo' => 'Id Grupo',
            'Precio' => 'Precio',
            'foto' => 'Foto',
        ];
    }

    /**
     * Gets query for [[IdGrupo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdGrupo()
    {
        return $this->hasOne(Grupos::class, ['IdGrupo' => 'IdGrupo']);
    }

    /**
     * Gets query for [[Ventas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVentas()
    {
        return $this->hasMany(Ventas::class, ['CodProducto' => 'IdProducto']);
    }
    
    public function getPrecioFinal(): string {
        return $this->precioFinal=$this->Precio . "€";
    }

    
    public function grupos(){
        return \yii\helpers\ArrayHelper::map(
            Grupos::find()->all(), // listar todos los grupos
            "IdGrupo",
            "NombreGrupo"
        );
    }
    
    public function gruposAutompletar(){
        return Grupos::find()->asArray()
                ->select("IdGrupo as value,NombreGrupo as label")
                ->all();       
    }
    
 
}

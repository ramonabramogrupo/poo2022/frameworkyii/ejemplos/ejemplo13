<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vendedores".
 *
 * @property int $IdVendedor
 * @property string|null $NombreVendedor
 * @property string|null $FechaAlta
 * @property string|null $NIF
 * @property string|null $FechaNac
 * @property string|null $Direccion
 * @property string|null $Poblacion
 * @property string|null $CodPostal
 * @property string|null $Telefon
 * @property string|null $EstalCivil
 * @property bool|null $activo
 *
 * @property Ventas[] $ventas
 */
class Vendedores extends \yii\db\ActiveRecord {

    private string $FechaAltaFinal;
    private string $FechaNacimiento;

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'vendedores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['FechaAlta', 'FechaNac'], 'safe'],
            [['activo'], 'boolean'],
            [['NombreVendedor', 'Direccion', 'Poblacion', 'Telefon'], 'string', 'max' => 50],
            [['NIF'], 'string', 'max' => 9],
            [['CodPostal'], 'string', 'max' => 5],
            [['EstalCivil'], 'string', 'max' => 15],
            [['NIF'], 'unique', 'message' => 'El {attribute} ya esta siendo utilizado'],
            [['NIF'], 'required'],
            [['NIF'], 'condicion'],
        ];
    }

    private function comprobarLetra() {
        $letras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];

        $dni = substr($this->NIF, 0, 8);
        $letra = strtoupper(substr($this->NIF, -1, 1));
        $salida = true; // la letra es correcta
        
        if ($dni < 0 || $dni > 99999999) {
            $salida = false; // la letra esta mal porque el dni no es correcto
        } else {
            // si el dni es valido compruebo la letra
            $resto = $dni % 23;
            $letraCalculada = $letras[$resto];
            if ($letraCalculada == $letra) {
                $salida = true;
            } else {
                $salida = false; // la letra es mal
            }
        }
        return $salida;
    }

    function condicion($atributo) {
        if (!$this->comprobarLetra()) {
            return $this->addError($atributo, "El NIF no es correcto");
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'IdVendedor' => 'Codigo',
            'NombreVendedor' => 'Nombre',
            'FechaAlta' => 'Fecha Alta',
            'NIF' => 'NIF',
            'FechaNac' => 'Fecha Nacimiento',
            'Direccion' => 'Direccion',
            'Poblacion' => 'Poblacion',
            'CodPostal' => 'Codigo Postal',
            'Telefon' => 'Telefono',
            'EstalCivil' => 'Estado Civil',
            'activo' => 'Activo',
        ];
    }

    /**
     * Gets query for [[Ventas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVentas() {
        return $this->hasMany(Ventas::class, ['CodVendedor' => 'IdVendedor']);
    }

    public function getFechaAltaFinal() {

        if (!empty($this->FechaAlta)) {
            return Yii::$app->formatter->asDate($this->FechaAlta, 'php:d/m/Y');
        }
    }

    public function getFechaNacimiento() {

        if (!empty($this->FechaNac)) {
            return Yii::$app->formatter->asDate($this->FechaNac, 'php:d/m/Y');
        }
    }

}
